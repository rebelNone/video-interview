import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {AgoraIndexComponent} from './agora-index.component';

describe('AgoraIndexComponent', () => {
    let component: AgoraIndexComponent;
    let fixture: ComponentFixture<AgoraIndexComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            declarations: [AgoraIndexComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AgoraIndexComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('AgoraIndexComponent should create', () => {
        expect(component).toBeTruthy();
    });
});
