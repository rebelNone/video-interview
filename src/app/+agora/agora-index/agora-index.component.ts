import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnDestroy,
    OnInit
} from '@angular/core';
import {ApiService} from '../shared/services/api/api.service';
import {AgoraService} from '../shared/services';
import {BaseComponent} from '../../base/base.component';
import {CHANNEL_NAME} from '../shared/services/agora.service';


@Component({
    selector: 'app-agora-index',
    templateUrl: './agora-index.component.html',
    styleUrls: ['./agora-index.component.scss'],
    changeDetection: ChangeDetectionStrategy.Default
})
export class AgoraIndexComponent extends BaseComponent implements OnInit, OnDestroy {
    @Input() public user;
    @Input() public isActiveMicrophone;

    constructor(public agora: AgoraService,
                protected api: ApiService,
                protected readonly cdRef: ChangeDetectorRef) {
        super();

        this.isActiveMicrophone = false;
        this.isError = false;
    }

    public isHiddenButtons = true;
    public isError: boolean;

    public ngOnInit(): void {
        this.agora.initialization();

        this.startVideoCall();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this.cdRef.detach();
    }

    public async startVideoCall(): Promise<any> {
        const uid = this.agora.generateUid();
        const recordId = this.agora.generateUid();

        const rtcDetails = await this.agora.generateTokenAndUid(uid);

        await this.agora.startVideoCall(rtcDetails.token, rtcDetails.uid);

        await this.agora.createUser(CHANNEL_NAME, rtcDetails.token, recordId).subscribe(r => console.log(r));

        this.isActiveMicrophone = true;
        this.isHiddenButtons = false;
        this.isError = false;
    }

    public async leaveVideoCall(): Promise<void> {
        await this.agora.leaveCall();
    }
}
