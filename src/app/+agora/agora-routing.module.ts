import { RouterModule, Routes } from '@angular/router';
import { AgoraIndexComponent } from './agora-index/agora-index.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: AgoraIndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgoraRoutingModule {}
