import { NgModule } from '@angular/core';
import {NbButtonModule} from '@nebular/theme';
import {CommonModule} from '@angular/common';
import { AgoraRoutingModule } from './agora-routing.module';
import {AgoraIndexComponent} from './agora-index/agora-index.component';
import {CheckConnectionComponent} from './check-connection/check-connection.component';

import {AgoraService} from './shared/services';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  imports: [
    AgoraRoutingModule,
    BrowserModule,
    NbButtonModule,
    CommonModule,
  ],
  providers: [AgoraService ],
  declarations: [
    AgoraIndexComponent,
    CheckConnectionComponent,
  ]
})
export class AgoraModule {}
