import { Component, OnInit } from '@angular/core';
import AgoraRTC from 'agora-rtc-sdk-ng';
import {DeviceTypes} from './check-connection.model';
import {AgoraRTCService} from '../shared/services';


@Component({
  selector: 'app-check-connection',
  templateUrl: './check-connection.component.html',
  styleUrls: ['./check-connection.component.scss']
})
export class CheckConnectionComponent implements OnInit {
  private microphoneTrack: any;

  constructor(private rtcClient: AgoraRTCService) { }

  public ngOnInit(): void {
      this.getDevices();
  }

  public getDevices(): void {
      this.rtcClient.getDevices()
          .then(devices => {
              const audioDevices = devices.filter((device: MediaDeviceInfo) => device.kind === DeviceTypes.Audio);
              const videoDevices = devices.filter((device: MediaDeviceInfo) => device.kind === DeviceTypes.Video);

              if (!videoDevices.length) {
                  throw new Error('videoDevices is empty');
              }

              if (!audioDevices.length) {
                  throw new Error('audioDevices is empty');
              }

              const selectedMicrophoneId = audioDevices[0].deviceId;
              const selectedCameraId = videoDevices[0].deviceId;
              return Promise.all([
                  AgoraRTC.createCameraVideoTrack({ cameraId: selectedCameraId }),
                  AgoraRTC.createMicrophoneAudioTrack({ microphoneId: selectedMicrophoneId }),
              ]);
          }).then(([videoTrack, audioTrack]) => {
          videoTrack.play('local-player', {fit: "contain"});
          setInterval(() => {
              const level = audioTrack.getVolumeLevel();
              console.log('local stream audio level', level);
          }, 1000);
      });
  }

  public changeMicrophone(): any {
    AgoraRTC.onMicrophoneChanged = async (changedDevice) => {
      // When plugging in a device, switch to a device that is newly plugged in.
      if (changedDevice.state === 'ACTIVE') {
        // microphoneTrack.setDevice(changedDevice.device.deviceId);
        // Switch to an existing device when the current device is unplugged.
      } else if (changedDevice.device.label === this.microphoneTrack.getTrackLabel()) {
        const oldMicrophones = await AgoraRTC.getMicrophones();
        // oldMicrophones[0] && microphoneTrack.setDevice(oldMicrophones[0].deviceId);
      }
    };
  }

    /*    public async switchCamera(label, localTracks): Promise<void> {
        this.cams = await AgoraRTC.getCameras();
        this.currentCam = this.cams.find(cam => cam.label === label);
        await this.client.localVideoTrack.setDevice(this.currentCam.deviceId);
      }*/
}
