import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';

import { AgoraService } from './agora.service';

describe('AgoraService', () => {
  let service: AgoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AgoraService]
    });
    service = TestBed.inject<AgoraService>(AgoraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
