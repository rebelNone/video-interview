import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import AgoraRTC, {ConnectionState, IAgoraRTCClient, IAgoraRTCRemoteUser} from 'agora-rtc-sdk-ng';
import {BehaviorSubject, Observable, Subscriber} from 'rxjs';
import {authorizationField} from '../../../base/api.config';
import {
    AgoraCloudRecordAcquireRequestBody,
    AgoraCloudRecordAcquireResponse, AgoraCloudRecordQueryResponse,
    AgoraCloudRecordStartedRequestBody, AgoraCloudRecordStartedResponse,
    AgoraOptions, agoraStatusTypes,
    AgoraUser, RTCSettings,
} from './agora.model';
import {environment} from '../../../../environments/environment';
import {BaseService} from '../../../base/base.service';
import {retry, tap} from 'rxjs/operators';
import {ApiService} from './api/api.service';
import {recordingConfig} from '../../../base/recording.config';
import {AgoraRTCService} from './rtc-client/agora-rtc.service';
import {AdoraMediaTypes} from './agora.types';

// TODO: replace variables
export const APP_ID = '80c96c653faa4d9ebab0ac20bc8b864b';
export const CHANNEL_NAME = 'demo_channel_name';
const TOKEN = '00680c96c653faa4d9ebab0ac20bc8b864bIACIoalCGpoKjdZW1tZgTRHoMRz6bUwLhy2kxO0EHhSUq44kO3kAAAAAEAADESomaXAfYQEAAQBpcB9h';

export class UserState {
    public connectionState!: ConnectionState;
    public mediaType?: AdoraMediaTypes.Audio | AdoraMediaTypes.Video;
    public user!: AgoraUser;
}
/**
 * AgoraService
 * @description
 * Note the resourceId is akin to a token in the sense that it has a lifetime of 5 minutes,
 * afterwards it expires and you have to call acquire for a new resourceId
 */
@Injectable({
    providedIn: 'root'
})
export class AgoraService extends BaseService {
    constructor(protected http: HttpClient, protected api: ApiService, protected rtcClient: AgoraRTCService) {
        super(http);
    }
    public remoteUsers: AgoraUser[] = [];
    public rtc: RTCSettings = {
        client: null,
        // For the local audio and video tracks.
        localAudioTrack: null,
        localVideoTrack: null,
    };

    public updateUserInfo = new BehaviorSubject<any>(null); // to update remote users name

    public cloudRecordData = null;

    public mics = [];
    public cams = [];
    public token;
    public client: IAgoraRTCClient;

    private resourceId?: string;
    private recordId?: string;
    acquireID: any;
    private uid: string;
    private _recordingConfig = recordingConfig;
    private _userStateEvent: EventEmitter<UserState> = new EventEmitter();
    private mode = 'mix';
    public options: AgoraOptions = {
        appId: APP_ID,
        channel: CHANNEL_NAME,
        /** Pass a token if your project enables the App Certificate. */
        token: TOKEN,
        uid: null,
    };
    private _baseQueryUrl = `${environment.baseUrl + APP_ID}/cloud_recording`;

    private static getAuthorizationHeaders(): any {
        return {
            headers: new HttpHeaders({
                Authorization: authorizationField,
                'Content-Type': 'application/json;charset=utf-8',
            }),
        };
    }

    private static _connectionLog(state): void{
        switch (state) {
            case 'CONNECTED':
                console.log('CONNECTED');
                break;
            case 'CONNECTING':
                console.log('CONNECTING');
                break;
            case 'DISCONNECTING':
                console.log('DISCONNECTING');
                break;
            case 'DISCONNECTED':
                console.log('DISCONNECTED');
                break;
        }
    }

    private static _queryResponseChecker(response): void {
        console.log('fileList: ', response.serverResponse.fileList);
        console.log('status: ', response.serverResponse.status, agoraStatusTypes[response.serverResponse.status]);
    }

    public initialization(): void {
        this.rtc.client = this.rtcClient.create();
        this._getNewUserData(this.rtc.client);
    }

    public async startVideoCall(token, uuid): Promise<number | string> {
        if (token !== null) {
            try {
                const uid = await this.rtc.client.join(this.options.appId, this.options.channel, token, uuid);

                this.publishStream()
                    .catch(err => console.log('publishStream error: ', err));

                this.remoteUsers.push({hasAudio: true, hasVideo: true, uid} as AgoraUser);

                return uid;
            } catch (err) {
                throw new Error(err);
            }
        }
    }

    public createUser(channelName, token, uid): Observable<unknown> {
        return new Observable<unknown>((observer) => {
            this._acquire(uid)
                .pipe(retry(10))
                .subscribe(
                    (response) => {
                        if (response.resourceId != null) {

                            this._start(uid, APP_ID, response.resourceId, channelName, TOKEN, this.mode)
                                .pipe(retry(10))
                                .subscribe(
                                    (resp) => {
                                        observer.next(resp);
                                        observer.complete();
                                    },
                                    (err) => {
                                        this.leaveCall().then(() => {
                                            observer.error();
                                        });
                                    }
                                );
                        }
                    },
                    (err) => {
                        this.leaveCall().then(() => { console.log(err); });
                    }
                );
        });
    }

    public async publishStream(): Promise<void> {
        this.rtc.localAudioTrack = await AgoraRTC.createMicrophoneAudioTrack();
        this.rtc.localVideoTrack = await AgoraRTC.createCameraVideoTrack();

        if (!this.rtc.localAudioTrack || !this.rtc.localVideoTrack) {
            [ this.rtc.localAudioTrack, this.rtc.localVideoTrack ] = await Promise.all([
                AgoraRTC.createMicrophoneAudioTrack(),
                AgoraRTC.createCameraVideoTrack()
            ]);
        }

        this.rtc.localAudioTrack.play();
        this.rtc.localVideoTrack.play('local-player', {fit: 'contain'});

        await this.rtc.client.publish([this.rtc.localAudioTrack, this.rtc.localVideoTrack]);
    }

    public generateUid(): number {
        const length = 5;
        return (Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1)));
    }

    private _getNewUserData(client): void {
        client.on('user-published', async (
                user: IAgoraRTCRemoteUser, mediaType: AdoraMediaTypes.Audio | AdoraMediaTypes.Video
            ) => {
            console.log(user);

            if (mediaType === AdoraMediaTypes.Audio) {
                const remoteAudioTrack = user.audioTrack;
                remoteAudioTrack.play();
            }

            if (mediaType === AdoraMediaTypes.Video) {
                const remoteVideoTrack = user.videoTrack;
                remoteVideoTrack.play('remote-playerlist' + user.uid, {fit: 'contain'});
            }

            await this.rtc.client.subscribe(user, mediaType).then(pr => console.log('VIDEOTRACK:', pr));
        });

        client.enableAudioVolumeIndicator();
        client.on('volume-indicator', async (result) => {
/*            result.forEach((volume, index) => {
                console.log(`${index} UID ${volume.uid} Level ${volume.level}`);
            });*/
        });

        client.on('user-left', user => {
            this._userStateEvent.emit({ connectionState: 'DISCONNECTING', user });
        });

        client.on('user-unpublished', async user => {
            console.log(user, 'user-unpublished');
        });

        client.on('error', async err => {
            console.log('Got error msg:', err.reason);
            if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
                await client.renewChannelKey('', () => {
                    console.log('Renew channel key successfully');
                }, (error) => {
                    console.log('Renew channel key failed: ', error);
                });
            } else if (err.reason === 'CAN_NOT_PUBLISH_MULTIPLE_VIDEO_TRACKS') {
                console.log('CAN_NOT_PUBLISH_MULTIPLE_VIDEO_TRACKS');
            } else if (err.reason === 'NO_ICE_CANDIDATE') {
                console.log('NO_ICE_CANDIDATE');
            }
        });

        // client.on('user-joined', this.anyFunc());
        client.on('user-joined', async user => {
            const id = user.uid;
            console.log(`${user} joined`);
            this.rtc.client.remoteUsers.push({hasAudio: true, hasVideo: true, uid: id});
            await this.updateUserInfo.next(id);
        });


        // tslint:disable-next-line:only-arrow-functions
        client.on('connection-state-change', async function(state): Promise<void> {
            await AgoraService._connectionLog(state);
        });
    }

     /**
      * When token-privilege-will-expire occurs,
      * fetch a new token from the server and call renewToken to renew the token.
      */
    private _getNewToken(uid): void {
        this.client.on('token-privilege-will-expire', async () => {
            const token = await this.generateTokenAndUid(uid);
            await this.client.renewToken(token);
        });
    }

    /**
     * When token-privilege-did-expire occurs,
     * fetch a new token from the server and call join to rejoin the channel.
     */
    private _getNewTokenPrivilege(client): void {
        client.on('token-privilege-did-expire', async (user: IAgoraRTCRemoteUser) => {
            console.log('Fetching the new Token');
            const token = await this.generateTokenAndUid(user.uid);
            console.log('Rejoining the channel with new Token');
            await this.rtc.client.join(this.options.appId, this.options.channel, token, user.uid);
        });

    }

    public async generateTokenAndUid(uid): Promise<any> {
        // https://test-agora.herokuapp.com/access_token?channel=test&uid=1234
        const url = 'https://test-agora.herokuapp.com/access_token?';
        const options = {
            params: new HttpParams({
                fromString: `channel=${this.options.channel}&uid=` + uid
            })
        };
        const data = await this.api.getRequest(url, options.params).toPromise();
        return {uid, token: data.token};
    }

    public async updateToken(token): Promise<any> {
        await this.client.renewToken(token);
        await this.rtc.client.renewToken(token);
    }

    private _acquire(uid): Observable<AgoraCloudRecordAcquireResponse> {
        console.log(this.options.channel, uid);
        const url = `${this._baseQueryUrl}/acquire`;

        const bodyRequest: AgoraCloudRecordAcquireRequestBody = {
            cname: this.options.channel,
            uid: uid.toString(),
            clientRequest: {
                resourceExpiredHour:  24,
                /**
                 * scene (Optional) Number. Sets the recording options.
                 * 0(default) indicates allocating resources for video and audio recording in a channel.
                 * 1 indicates allocating resources for web page recording
                 */
            }
        };

        return new Observable<AgoraCloudRecordAcquireResponse>((observer: Subscriber<any>) => {
            this.http.post(url, bodyRequest, AgoraService.getAuthorizationHeaders())
                .subscribe(
                    (resourceId) => {
                        if (resourceId) {
                            this.acquireID = uid.toString();
                            console.log('this.acquireID', this.acquireID);
                            observer.next(resourceId);
                            observer.complete();
                        } else {
                            throw {
                                recordingErr: {
                                    message: 'acquire recording failed',
                                },
                                reason: 'resourceId is invalid',
                            };
                        }
                    },
                    (err) => {
                        observer.error(err);
                    }
                );
        });
    }

    private _start(uid, appid, resourceId, channelName, token, mode = 'mix'): Observable<any> {
        console.log('start', uid);
        if (typeof resourceId === 'undefined') {
            throw {
                recordingErr: {
                    message: 'start recording failed',
                },
                reason: 'resourceId is undefined',
            };
        }

        this.resourceId = resourceId;
        this.uid = uid;
        const url = `${this._baseQueryUrl}/resourceid/${resourceId}/mode/${mode}/start`;
        const fileNamePrefix = channelName.split('_').join('');

        const bodyRequest: AgoraCloudRecordStartedRequestBody = {
            cname: channelName,
            uid: uid.toString(),
            clientRequest: {
                token: this.token,
                recordingConfig: this._recordingConfig,
                recordingFileConfig: {
                    avFileType: ['hls', 'mp4'], // ['hls']
                },
                storageConfig: {
                    vendor: 1, // Amazon S3
                    region: 4, // EU_CENTRAL_1
                    bucket: 'agorabucketaws',
                    accessKey: 'AKIASQDUOAXRFDRT36OK', // this.accessKey,
                    secretKey: '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', // this.secretKey,
                    fileNamePrefix: [fileNamePrefix],
                },
            },
        };
        return new Observable<any>((observer) => {
            this.http.post(url, bodyRequest, AgoraService.getAuthorizationHeaders())
                .subscribe(
                    // tslint:disable-next-line:no-shadowed-variable
                    (resourceId) => {
                        observer.next(resourceId);
                        observer.complete();
                        this._query(resourceId);
                    },
                    (err) => {
                        observer.error(err);
                    }
                );
        });
    }

    private _update(): void {
        const url = `${this._baseQueryUrl}/resourceid/${this.resourceId}/sid/${this.recordId}/mode/${this.mode}/update`;
        const body = {
            uid: this.uid.toString(), // '527841',
            cname: this.options.channel,
            clientRequest: {
                streamSubscribe: {
                    audioUidList: {
                        subscribeAudioUids: ['#allstream#']
                    },
                    videoUidList: {
                        unSubscribeVideoUids: ['444', '555', '666']
                    }
                }
            }
        };
    }

    // public setVolumeWave() {
    //   let volumeAnimation = requestAnimationFrame(setVolumeWave);
    //   // this.rtc.localAudioTrack.getVolumeLevel() * 100;
    // }

    private async _query(resourceId): Promise<any> {
        try {
            if (typeof resourceId.sid === 'string') {
                this.recordId = resourceId.sid;
            } else {
                throw {
                    recordingErr: {
                        message: 'start recording failed',
                    },
                    reason: 'recordId is invalid',
                };
            }
            const response = await this.http.get(
                `${this._baseQueryUrl}/resourceid/${resourceId.resourceId}/sid/${resourceId.sid}/mode/${this.mode}/query`,
                AgoraService.getAuthorizationHeaders()
            );
            response.pipe(
                tap((val) => {
                    console.log('RESPONSE:', val);
                })
            );
            return response.subscribe(
                (res) => {
                    if (res) {
                        AgoraService._queryResponseChecker(res);
                    }
                });
        } catch (err) {
            console.log(err);
        }
    }

    public stop(uid): any {
        if (typeof this.resourceId === 'undefined') {
            throw {
                recordingErr: {
                    message: 'stop recording failed',
                },
                reason: 'resourceId is undefined',
            };
        }
        if (typeof this.recordId === 'undefined') {
            throw {
                recordingErr: {
                    message: 'stop recording failed',
                },
                reason: 'recordId is undefined',
            };
        }
        try {
            const header = new HttpHeaders().set(
                'Authorization',
                authorizationField
            ).set(
                'Content-Type',
                'application/json;charset=utf-8'
            );
            return this.http.post(
                `${this._baseQueryUrl}/resourceid/${this.resourceId}/sid/${this.recordId}/mode/${this.mode}/stop`,
                {
                        cname: this.options.channel,
                        uid: uid.toString(),
                        clientRequest: {
                            // async_stop: false
                        }
                    },
                {
                    headers: header,
                }
            );
        } catch (err) {
            console.log('stop', err);
        } finally {
            this._release();
        }
    }

    private async _release(): Promise<void> {
        this.resourceId = undefined;
        this.recordId = undefined;
    }

    public async leaveCall(): Promise<void> {
        this.rtc.localAudioTrack.close();
        this.rtc.localVideoTrack.close();

        this.rtc.client.remoteUsers.forEach(userItem => {
            this.rtc.client.unsubscribe(userItem);
            const playerContainer = document.getElementById('remote-playerlist' + userItem.uid.toString());
            const _ = playerContainer && playerContainer.remove();
        });

        this.stop(this.uid).subscribe((data: (any)) => {
            console.log('>>> RECORD OFF', data);
        });

        // await this.client.unpublish(); // TODO: rtcClient
        // await this.rtcClient.exit();
        await this.rtc.client.leave();
    }
}
