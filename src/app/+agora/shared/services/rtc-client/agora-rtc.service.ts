import {BaseService} from '../../../../base/base.service';
import {Injectable} from '@angular/core';
import AgoraRTC, {ClientConfig, IAgoraRTCClient} from 'agora-rtc-sdk-ng';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../api/api.service';

const CONFIG_RTC: ClientConfig = {mode: 'rtc', codec: 'vp8', role: 'host'};

@Injectable({
    providedIn: 'root'
})
export class AgoraRTCService extends BaseService {
    constructor(protected http: HttpClient, protected api: ApiService) {
        super(http);

        this._clientEvents = [];
    }

    private _clientEvents: any;
    public client: IAgoraRTCClient;

    set _client(value: IAgoraRTCClient) {
        this.client = value;
    }

    /**
     * createRTCClient
     * @description
     * Params:
     * config – The configurations for the AgoraRTC object, including channel profile and codec.
     * The default codec is 'vp8' and default channel profile is 'rtc'.
     */
    create(): IAgoraRTCClient {
        this._client = AgoraRTC.createClient(CONFIG_RTC);
        return this.client;
    }

    // async join(uid: string, channel: string, token?: string): Promise<unknown> {
    //     return new Promise((resolve, reject) => {
    //         this._client.join(token, channel, uid);
    //     });
    // }

/*    subscribe(user: IAgoraRTCRemoteUser): void {
        this._client.subscribe(user, {video: true, audio: true});
    }*/

    getDevices(): Promise<MediaDeviceInfo[]> {
        return AgoraRTC.getDevices();
    }

    setAudioOutput(speakerId: string): Promise<any> {
        return new Promise((resolve, reject) => {
           // ? this._client.setAudioOutput(speakerId, resolve, reject);
        });
    }

    setAudioVolume(volume: number): void {
        // add? this._client.setAudioVolume(volume);
    }

    renewToken(newToken: string): void {
        if (!this.client) {
            return console.warn('renewToken is not permitted, checkout your instance');
        }
        this.client.renewToken(newToken);
    }

    async exit(): Promise<any> {
        try {
            await this.leave();
        } catch (err) {
            throw err;
        } finally {
            await this.destroy();
        }
    }

    async leave(): Promise<any> {
        if (this.client) {
            return new Promise((resolve, reject) => {
                this.client.leave();
            });
        }
    }

    destroy(): void {
        this.unsubscribeClientEvents();
    }

    unsubscribeClientEvents(): void {
        for (const evtName of this._clientEvents) {
            this.client.off(evtName, () => {});
            this._clientEvents = this._clientEvents.filter((it: any) => it === evtName);
        }
    }
}
