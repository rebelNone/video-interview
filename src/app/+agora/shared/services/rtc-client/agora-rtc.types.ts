export const streamEvents: string[] = [
    'accessAllowed',
    'accessDenied',
    'stopScreenSharing',
    'videoTrackEnded',
    'audioTrackEnded',
    'player-status-changed'
];

export const clientEvents: string[] = [
    'stream-published',
    'stream-added',
    'stream-removed',
    'stream-subscribed',
    'peer-online',
    'peer-leave',
    'error',
    'network-type-changed',
    'network-quality',
    'exception',
    'onTokenPrivilegeWillExpire',
    'onTokenPrivilegeDidExpire',
];
