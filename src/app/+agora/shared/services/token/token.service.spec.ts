import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {TokenService} from './token.service';

describe('TokenService', () => {
    let service: TokenService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [TokenService]
        });
        service = TestBed.inject<TokenService>(TokenService);
    });

    afterEach(() => localStorage.clear());

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get the item TokenService', () => {
        service.setToken('TEST');
        expect(service.getToken()).toBe('TEST');
    });

    it('should remove the item TokenService', () => {
        service.removeToken();
        expect(service.getToken()).toBe(null);
    });
});
