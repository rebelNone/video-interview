import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from '../../../../../environments/environment';
import { BaseService } from '../../../../base/base.service';
import { AgoraService } from '../agora.service';

/**
 * TokenService
 * @description
 * Manipulate with localStorage.
 */
@Injectable({
    providedIn: 'root'
})
export class TokenService extends BaseService {

    constructor(protected http: HttpClient,
                public agora: AgoraService) {
        super(http);
    }

    static hasToken(): boolean {
        return !!localStorage.getItem(environment.token);
    }

    public getToken(): string {
        return localStorage.getItem(environment.token);
    }

    public setToken(token: string): void {
        localStorage.setItem(environment.token, token);
    }

    // TODO: JWT

    public removeToken(): void {
        if (TokenService.hasToken()) {
            localStorage.removeItem(environment.token);
        }
    }

    public isUserLoggedIn(): boolean {
        return (TokenService.hasToken() && !this.isTokenExpired());
    }

    public getTokenExpirationDate(token: string = this.getToken()): Date | null {
        let decoded: any;
        decoded = TokenService.parseJWT(token);

        if (!decoded.hasOwnProperty('exp')) {
            return null;
        }

        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);

        return date;
    }

    public isTokenExpired(token: string = this.getToken(), offsetSeconds?: number): boolean {
        if (token === null || token === '') {
            return true;
        }
        const date = this.getTokenExpirationDate(token);
        offsetSeconds = offsetSeconds || 0;

        if (date === null) {
            return true;
        }

        return !(date.valueOf() > new Date().valueOf() + offsetSeconds * 1000);
    }

    private static parseJWT(token: string): any {
        const base64Url = token.split('.')[1];
        const base64 = base64Url
            .replace('-', '+')
            .replace('_', '/');

        return JSON.parse(window.atob(base64));
    }
}
