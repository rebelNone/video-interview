import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AgoraIndexComponent} from './+agora/agora-index/agora-index.component';

const routes: Routes = [
  // {
  //   path: '',
  //   pathMatch: 'full',
  //   redirectTo: 'dashboard'
  // },
  {
    path: 'user/:id',
    component: AgoraIndexComponent,
  },
  // {
  //   path: 'agora',
  //   loadChildren: () => import('./+agora/agora.module').then(m => m.AgoraModule),
  //   // canLoad: [ AuthGuards ]
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
