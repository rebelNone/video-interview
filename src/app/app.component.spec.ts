import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {Router} from '@angular/router';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let router: Router;
  const routerSpy = {navigate: jasmine.createSpy('navigate')};

  beforeEach(async (() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent ],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [ { provide: Router, useValue: routerSpy } ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create AppComponent', () => {
    expect(component)
        .toBeTruthy();
  });

  it('should go to user page', () => {
    const value = 1;
    component.open(value);
    expect (routerSpy.navigate)
        .toHaveBeenCalledWith([`/user/${value}`]);
  });
});
