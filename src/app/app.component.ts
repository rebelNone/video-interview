import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {BaseComponent} from './base/base.component';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {environment} from '../environments/environment';


const TEXT_CONNECT = 'Подключиться';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit {

  public isHidden = true;
  public textConnect = TEXT_CONNECT;
  public iframeSrc: SafeUrl;

  @ViewChild('iframe', { static: true }) public iframeRef: ElementRef;
  @HostListener('window:message', ['$event'])
  public onMessage(event): void {
    this.receiveMessage(event);
  }

  constructor(private readonly router: Router,
              private readonly sanitizer: DomSanitizer) {
    super();
    // this.iframeSrc = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.baseIframeUrl}`);
  }

  public ngOnInit(): void {}

  // should be dynamic uid for user
  public open(value): void{
    this.router.navigate([`/user/${value}`]).then(r => console.log('navigate', `/user/${value}`));
    this.isHidden = false;
  }

  private receiveMessage(event): void {
    if (event.origin !== environment.targetOrigin) {
      return;
    }

    // should be more cases
    if (event.data.type) {
      const data = event.data;
      switch (data.type) {
        case 'auth':
          if (data.value === true) {
            this.iframeSrc = this.iframeSrc;
          }
          break;
      }
    }
  }
}
