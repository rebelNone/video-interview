import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NbButtonModule, NbLayoutModule, NbThemeModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import {CommonModule} from '@angular/common';
import {AgoraModule} from './+agora/agora.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AgoraModule,
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NbButtonModule,
    NbEvaIconsModule,
    NbLayoutModule,
    NbThemeModule.forRoot({name: 'default'}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
