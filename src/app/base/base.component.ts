import { Component, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';

@Component({
    template: ''
})
// TODO: Add Angular decorator.
export abstract class BaseComponent implements OnDestroy {
    protected ngUnsubscribe: Subject<void>;

    protected constructor() {
        this.ngUnsubscribe = new Subject<void>();
    }

    public ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
