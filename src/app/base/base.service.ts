import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

/**
 * BaseService
 * @description
 * Contains the basic functionality for services such as `apiUrl` and provide `HttpClient`.
 * All services that can send data to server should extend this class
 * @example
 * ```
 * class SomeService extends BaseService {
 *   method() {
 *     return this.http.get(`${this.apiUrl}/some`)
 *   }
 * }
 * ```
 */
@Injectable()
export abstract class BaseService {
    protected readonly apiUrl = environment.apiUrl;
    protected readonly baseUrl = environment.baseUrl;
    protected constructor(protected readonly http: HttpClient) {}
}
