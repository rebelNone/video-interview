
export const recordingConfig = {
    maxIdleTime: 10,
    /** streamTypes
     * 0: Subscribes to audio streams only.
     * 1: Subscribes to video streams only.
     * 2: (Default) Subscribes to both audio and video streams.
     */
    // streamTypes: 2,
    // audioProfile: 1,
    /** channelType
     * 0: (Default) Communication profile.
     * 1: Live broadcast profile.
     */
    // channelType: 0,
    /** videoStreamType (Optional) Number. The type of the video stream to subscribe to.
     * 0: (Default) Subscribes to the high-quality stream.
     * 1: Subscribes to the low-quality stream.
     */
    // videoStreamType: 0,
    // subscribeUidGroup: 0, // 1
   /* transcodingConfig: {
        width: 640,
        height: 360,
        fps: 25,
        bitrate: 1000,
        mixedVideoLayout: 1, // or 2
        maxResolutionUid: '0',
        backgroundColor: '#FF0000',
    }*/
};
