import { NgModule } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MOCK_ROUTES } from './router.mock';
import {MockActivatedRoute} from './activated-route.mock';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(MOCK_ROUTES)
    ],
    exports: [
    ],
    providers: [
        {
            provide: ActivatedRoute,
            useClass: MockActivatedRoute
        }
    ]
})
export class BaseTestingModule {}
