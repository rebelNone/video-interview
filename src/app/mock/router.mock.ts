class MockComponent {}

export const MOCK_ROUTES = [
    {
        path: 'user/:id',
        component: MockComponent
    },
];
